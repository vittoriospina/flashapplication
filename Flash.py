import 	pygame
import 	thread
import 	time 
import 	os
import 	sys
from 	pygame.locals import *

def PrintUsage():
	print "Application USAGE:"
	print 
	print " Flash.py [X] [Y] [W] [H]"
	print "   X application on screen position (X) [INT]"
	print "   Y application on screen position (Y) [INT]"
	print "   W application width [INT]"
	print "   H application height [INT]"
	print 
	print " e.a."
	print " Flash.py 0 0 1920 1080"
	print " # Full screen application on FullHD monitor"

def RenderText():
	global font
	global period, fontsize, flashON, InitialTextPosition
	textLineList = [		
						"FLASH Application",
						"Flash Period Lenght = "+str(period),
						"FlashStatus = "+str(flashON),
						"-----------------------",
						"Keyboard shortcuts:",
						"   +   : increment period lenght (+0.1)",
						"   -   : decrement period lenght (-0.1)",
						"   ESC : application exit"
	]	
	if flashON: 	textColor = [0,0,0]
	else: 			textColor = [255,255,255]
	textposition = list(InitialTextPosition)
	for line in textLineList:
		textposition[0] = InitialTextPosition[0]
		textposition[1] += fontsize
		TextLineSurface = font.render(line, True, textColor)
		screen.blit(TextLineSurface, textposition)

def RenderThread():
	global loop, font, period, flashON
	PassedTime  = time.time()	
	while loop:
		if time.time()-PassedTime>=period: flashON = not flashON; PassedTime = time.time()
		if flashON: 	screen.fill([255,255,255])
		else: 			screen.fill([0,0,0])
		RenderText()
		pygame.display.update()
		time.sleep(.00001)
#loading argv settings
try:
	x = int(sys.argv[1])
	y = int(sys.argv[2])
	w = int(sys.argv[3])
	h = int(sys.argv[4])
except Exception, e:
	print "FlashAPP Attribute ERROR: "+str(e)
	print 
	PrintUsage()
	sys.exit()

InitialTextPosition = [10,10]

pygame.init()
try: #Environment initialization
	os.environ['SDL_VIDEO_WINDOW_POS']=str(x)+","+str(y)
	screen 		= pygame.display.set_mode((w,h), pygame.NOFRAME)
except Exception, e:
	print "FlashAPP Initialization ERROR: "+str(e)
	print 
	PrintUsage()
	sys.exit()

pygame.display.set_caption("EER FlashApp")
pygame.mouse.set_visible(False)
loop 		= True
fontsize 	= 20
font 		= pygame.font.Font(None, fontsize)
period 		= .5
flashON 	= False
thread.start_new_thread(RenderThread,())
while loop:
	MousePos = pygame.mouse.get_rel()
	for event in pygame.event.get():
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_PLUS	or event.key == K_KP_PLUS: 	period +=.1
			if event.key == pygame.K_MINUS	or event.key == K_KP_MINUS: period = round(max(.1, period-.1),1)
			if event.key == pygame.K_ESCAPE: loop=False
	time.sleep(0.00001)
time.sleep(1)
pygame.quit()