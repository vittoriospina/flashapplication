# README #
This README would normally document whatever steps are necessary to get your application up and running.

### Application Description ###
This application only flash the screen with specified period timing

### Usage ###
Flash.py [X] [Y] [W] [H]

    [X] Application on screen position (X) [INT]
   
    [Y] Application on screen position (Y) [INT]
   
    [W] Application width [INT]
   
    [H] Application height [INT]
   
e.a.

    Flash.py 0 0 1920 1080
   
    (for full screen application on FullHD monitor)
 
### Dipendencies ###
* python 2.7 [https://www.python.org/download/releases/2.7/] N.B. download always 32 bit version!
* pygame module [http://www.pygame.org/download.shtml]